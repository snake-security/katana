GOOS=linux make build

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Make build... PASS!"
else
  # houston we have a problem
  exit 1
fi

strip katana

cp -Rf katana /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
